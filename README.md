# Workspaces für TYPO3 10 LTS

TYPO3-Installation aus dem Zoominar "Workspaces in TYPO3 10 LTS" vom 25.03.2021

Direkt verwendbar mit Docker+DDEV.

1. Klone dieses Repository (oder download als zip)
2. ddev start
3. ddev composer install
4. ddev import-db < database.sql
5. ddev launch /typo3

Benutzername: admin\
Passwort: password
